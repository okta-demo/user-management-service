package com.sample.usermgmt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.okta.sdk.authc.credentials.TokenClientCredentials;
import com.okta.sdk.client.Client;
import com.okta.sdk.client.Clients;
import com.okta.sdk.resource.group.Group;
import com.okta.sdk.resource.user.User;
import com.okta.sdk.resource.user.UserBuilder;
import com.okta.sdk.resource.user.UserList;
import com.okta.sdk.resource.user.UserProfile;

@Service
public class UserManagementService {
	
	public Map<String, List<User>> getAllUsers(){
		Client client = getClient();
		UserList users = client.listUsers();
		Group admin = client.getGroup("00g2fznyfczwTRY7b357");//id for admin group
		UserList adminUsers = admin.listUsers();
		List<User> aU = adminUsers.stream().collect(Collectors.toList());
		List<String> names = new ArrayList<String>();
		List<User> nonAdmin = users.stream().filter(x -> !aU.contains(x)).collect(Collectors.toList());
		Map<String, List<User>> res = new HashMap<String, List<User>>();
		res.put("admin", aU);
		res.put("nonAdmin", nonAdmin);
		users.forEach(x -> names.add(x.getProfile().getFirstName()));
		return res;
	}
	
	public List<String> getGroups(){
		Client client = getClient();
		client.listGroups().forEach(x -> System.out.println(x.getId()+ "---"+x.getProfile().getName()));
		return new ArrayList<String>();
	}
	
	public boolean deprovisionUser(String userId) {
		Client client = getClient();
		try{
			client.getUser(userId).deactivate();
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean deleteUser(String userId) {
		Client client = getClient();
		try{
			client.getUser(userId).delete(Boolean.TRUE);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public boolean createUser(MyUserProfile body) {
		Client client = getClient();
		try{
			UserBuilder.instance().setEmail(body.getEmail()).setFirstName(body.getFirstName()).setLastName(body.getLastName()).buildAndCreate(client);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public User updateUser(String userId, UserProfile profile) {
		Client client = getClient();
		return client.getUser(userId).setProfile(profile);
	}
	
	public User getUser(String userId) {
		Client client = getClient();
		return client.getUser(userId);
	}
	
	public boolean assignUserToGroup(String userId, String groupId) {
		Client client = getClient();
		try {
			client.getUser(userId).addToGroup(groupId);
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
	
	public Client getClient() {
		return Clients.builder().setClientCredentials(new TokenClientCredentials("#################################################"))
		        .setOrgUrl("https://dev-100661.okta.com").build();
	}

}
