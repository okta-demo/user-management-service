package com.sample.usermgmt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.okta.sdk.resource.user.User;


@RestController
@CrossOrigin(origins = "*")
public class UserMgmtController {
	
	@Autowired
	private UserManagementService usermgmtService;
	
	@GetMapping("/name")
	public String getName() {
		return "Gopal";
	}
	
	@GetMapping("/users")
	@CrossOrigin
	public ResponseEntity<Map<String, List<User>>> getAllUsers(){
		return ResponseEntity.ok(usermgmtService.getAllUsers());
	}
	
	@PostMapping("/makemeadmin")
	@CrossOrigin
	public ResponseEntity<Map<String, Boolean>> makeMeAdmin(@RequestParam(name="userId") String userId){
		Map<String, Boolean> status = new HashMap<String, Boolean>();
		status.put("status", usermgmtService.assignUserToGroup(userId, "00g2fznyfczwTRY7b357"));
		return ResponseEntity.ok(status);
	}
	
	@PostMapping("/create")
	@CrossOrigin
	public ResponseEntity<Map<String, Boolean>> createUser(@RequestBody MyUserProfile user){
		Map<String, Boolean> status = new HashMap<String, Boolean>();
		status.put("status", usermgmtService.createUser(user));
		return ResponseEntity.ok(status);
	}

}
